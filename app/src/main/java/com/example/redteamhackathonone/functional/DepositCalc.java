package com.example.redteamhackathonone.functional;

public class DepositCalc {
    private double depositSum;
    private double interestRate;
    private double term;
    private double replenishmentPerMonth = 0;
    private double tax;

    private boolean capitalizationStatus;

    private DepositCalc(double depositSum, double interestRate) {
        this.depositSum = depositSum;
        this.interestRate = interestRate;
        capitalizationStatus = false;
    }

    public DepositCalc(double depositSum, double interestRate, double term, double replenishmentPerMonth, double tax, boolean capitalizationStatus) {
        this.depositSum = depositSum;
        this.interestRate = interestRate;
        this.term = term;
        this.replenishmentPerMonth = replenishmentPerMonth;
        this.tax = tax;
        this.capitalizationStatus = capitalizationStatus;
    }

    public double getMonthlyIncome() {
        double result = 0;
        if ((depositSum <= 0) || (interestRate <= 0) || (term <= 0)) {
            return 0;
        }
        if (capitalizationStatus || replenishmentPerMonth > 0) return 0;
        result = depositSum * interestRate / 100 / 12;
        return result - (result * (tax / 100));
    }

    public double getTotalIncomeInNMonths() {
        double result = 0;
        if ((depositSum <= 0) || (interestRate <= 0) || (term <= 0)) {
            return 0;
        }
        if (capitalizationStatus && replenishmentPerMonth <= 0) {
            double total = depositSum;
            for (int i = 0; i < term; i++) {
                total += total * (interestRate / 100 / 12);
            }
            result = total - depositSum;
            result = result - (result * (tax / 100));
        } else if (capitalizationStatus && replenishmentPerMonth > 0) {
            double total = depositSum;
            for (int i = 0; i < term; i++) {
                result += total * (interestRate / 100 / 12);
                total += replenishmentPerMonth;
            }
            result += total * (interestRate / 100 / 12);
            result = result - (result * (tax / 100));
        } else if (!capitalizationStatus && replenishmentPerMonth > 0) {
            double total = depositSum;
            result = total * (interestRate / 100 / 12);
            for (int i = 0; i < term - 1; i++) {
                total += replenishmentPerMonth;
                result += total * (interestRate / 100 / 12);
            }
            result = result - (result * (tax / 100));
        } else {
            result = getMonthlyIncome() * term;
        }
        return result;
    }

    public double getTotalAmount() {
        double result = 0;
        if ((depositSum > 0) && (interestRate > 0) && (term > 0) && (replenishmentPerMonth > 0)) {
            return depositSum + getReplenishmentAmount() + getTotalIncomeInNMonths();
        }
        if ((depositSum > 0) && (interestRate > 0) && (term > 0)) {
            return depositSum + getTotalIncomeInNMonths();
        }
        return result;
    }

    public double getReplenishmentAmount() {
        if ((term <= 0) || (replenishmentPerMonth <= 0)) return 0;
        else {
            return (term - 1) * replenishmentPerMonth;
        }
    }
}