package com.example.redteamhackathonone.functional;

import android.os.Bundle;

public interface InfoSender {
    public void acceptMessage (Bundle data);
}
