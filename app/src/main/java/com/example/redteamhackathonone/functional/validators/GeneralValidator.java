package com.example.redteamhackathonone.functional.validators;

import android.widget.EditText;

public final class GeneralValidator {
    private GeneralValidator() {
    }

    static public boolean validate(EditText init) {
        String inStr = init.getText().toString();

        if (inStr.isEmpty()) {
            return false;
        }
        Double inDouble = Double.parseDouble(inStr);

        if (inDouble < 0) {
            return false;

        }
        return true;
    }

    public static double validateDouble(EditText init) {
        double result = 0;
        if (validate(init)){
            return Double.parseDouble(init.getText().toString());
        }
        return result;
    }
}