package com.example.redteamhackathonone;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.redteamhackathonone.deposit_fragments.InfoOutDepositFragment;
import com.example.redteamhackathonone.functional.AbstractTextWatcher;
import com.example.redteamhackathonone.functional.InfoSender;
import com.example.redteamhackathonone.functional.validators.GeneralValidator;

public class DepositActivity extends AppCompatActivity implements InfoSender {

    private InfoOutDepositFragment infoOutDepositFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void acceptMessage(Bundle data) {
        infoOutDepositFragment = (InfoOutDepositFragment) getSupportFragmentManager()
                .findFragmentById(R.id.info_deposit_data_fragment);
        if (infoOutDepositFragment != null
        && infoOutDepositFragment.isInLayout()){
            infoOutDepositFragment.printConvertInfo(data);
        }
    }
}
