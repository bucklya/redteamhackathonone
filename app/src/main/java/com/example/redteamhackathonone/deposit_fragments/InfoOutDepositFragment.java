package com.example.redteamhackathonone.deposit_fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.redteamhackathonone.R;

public class InfoOutDepositFragment extends Fragment {

    public static final String EXTRA_MONTHLY_INCOME = "deposit_fragments.EXTRA_MONTHLY_INCOME";
    public static final String EXTRA_TERM = "deposit_fragments.EXTRA_TERM";
    public static final String EXTRA_GENERAL_INCOME_N_MONTHS = "deposit_fragments.EXTRA_GENERAL_INCOME_N_MONTHS";
    public static final String EXTRA_GENERAL_SUM_OF_DEPOSIT = "deposit_fragments.EXTRA_GENERAL_SUM_OF_DEPOSIT";
    public static final String EXTRA_TOTAL_AMOUNT = "deposit_fragments.EXTRA_TOTAL_AMOUNT";

    private TextView monthlyIncomeView;
    private TextView genIncomeNText;
    private TextView genIncomeNOut;
    private TextView genSumOfDeposit;
    private TextView totalAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_out_deposit, container, false);

        monthlyIncomeView = view.findViewById(R.id.monthly_income_text_view);
        genIncomeNText = view.findViewById(R.id.general_income_N_months_tv);
        genIncomeNOut = view.findViewById(R.id.general_income_text_view);
        genSumOfDeposit = view.findViewById(R.id.general_sumOfDeposits_text_view);
        totalAmount = view.findViewById(R.id.total_amount_text_view);


        return view;
    }

    public void printConvertInfo(Bundle data) {
        if (data != null) {
            printMonthlyIncome(data.getDouble(EXTRA_MONTHLY_INCOME));
            printGenIncomeN(data.getInt(EXTRA_TERM));
            printIncomeNOut(data.getDouble(EXTRA_GENERAL_INCOME_N_MONTHS));
            printGeneralSumOfDeposit(data.getDouble(EXTRA_GENERAL_SUM_OF_DEPOSIT));
            printTotalAmount(data.getDouble(EXTRA_TOTAL_AMOUNT));
        }
    }

    private void printTotalAmount(double totalAm) {
        if (totalAm <= 0){
            totalAmount.setText("0");
        }else {
            totalAmount.setText(String.format("%.2f",totalAm));
        }
    }

    private void printGeneralSumOfDeposit(double sumOfDeposit) {
        if (sumOfDeposit <= 0){
            genSumOfDeposit.setText("0");
        }else {
            genSumOfDeposit.setText(String.format("%.2f",sumOfDeposit));
        }
    }

    private void printIncomeNOut(double income) {
        if (income <= 0){
            genIncomeNOut.setText("0");
        }else{
            genIncomeNOut.setText(String.format("%.2f",income));
        }
    }

    private void printGenIncomeN(int term) {
        if (term <= 0){
            genIncomeNText.setText(String.format(
                    getResources().getString(R.string.general_income_string),0));
        }else{
            genIncomeNText.setText(String.format(
                    getResources().getString(R.string.general_income_string),term));
        }
    }

    private void printMonthlyIncome(double init) {
        if (init <= 0) {
            monthlyIncomeView.setText("0");
        } else {
            monthlyIncomeView.setText(String.format("%.2f",init));
        }
    }
}
