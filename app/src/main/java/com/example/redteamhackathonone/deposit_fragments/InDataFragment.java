package com.example.redteamhackathonone.deposit_fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.example.redteamhackathonone.functional.DepositCalc;
import com.example.redteamhackathonone.R;
import com.example.redteamhackathonone.functional.AbstractTextWatcher;
import com.example.redteamhackathonone.functional.InfoSender;
import com.example.redteamhackathonone.functional.validators.GeneralValidator;

public class InDataFragment extends Fragment {

    private EditText inAmountText;
    private EditText interestRateText;
    private EditText periodOfTimeText;
    private EditText depositReplenishmentText;
    private EditText taxInterest;

    private CheckBox capitalCh;

    private InfoSender dataSender;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            dataSender = (InfoSender) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Can not cast to InfoSender");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_in_data, container, false);

        inAmountText = view.findViewById(R.id.in_amount_text);
        interestRateText = view.findViewById(R.id.interest_rate_text);
        periodOfTimeText = view.findViewById(R.id.period_of_time_string);
        depositReplenishmentText = view.findViewById(R.id.deposit_string);
        taxInterest = view.findViewById(R.id.tax_percent_string);


        capitalCh = view.findViewById(R.id.chk_capitalization);

        inAmountText.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (GeneralValidator.validate(inAmountText)
                        && inStrLength != inAmountText.getText().toString().length()) {
                    inStrLength = inAmountText.getText().toString().length();
                    sendUotData();
                }
                if (!GeneralValidator.validate(inAmountText) && inStrLength > 0) {
                    inAmountText.setText("0");
                    inStrLength = 0;
                }
            }
        });

        interestRateText.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (GeneralValidator.validate(interestRateText)
                        && inStrLength != interestRateText.getText().toString().length()) {
                    inStrLength = interestRateText.getText().toString().length();
                    sendUotData();
                }
                if (!GeneralValidator.validate(interestRateText) && inStrLength > 0) {
                    interestRateText.setText("0");
                    inStrLength = 0;
                }
            }
        });

        periodOfTimeText.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (GeneralValidator.validate(periodOfTimeText)
                        && inStrLength != periodOfTimeText.getText().toString().length()) {
                    inStrLength = periodOfTimeText.getText().toString().length();
                    sendUotData();
                }
                if (!GeneralValidator.validate(periodOfTimeText) && inStrLength > 0) {
                    periodOfTimeText.setText("0");
                    inStrLength = 0;
                }
            }
        });

        depositReplenishmentText.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (GeneralValidator.validate(depositReplenishmentText)
                        && inStrLength != depositReplenishmentText.getText().toString().length()) {
                    inStrLength = depositReplenishmentText.getText().toString().length();
                    sendUotData();
                }
                if (!GeneralValidator.validate(depositReplenishmentText) && inStrLength > 0) {
                    depositReplenishmentText.setText("0");
                    inStrLength = 0;
                }
            }
        });

        taxInterest.addTextChangedListener(new AbstractTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (GeneralValidator.validate(taxInterest)
                        && inStrLength != taxInterest.getText().toString().length()) {
                    double tax = GeneralValidator.validateDouble(taxInterest);
                    if (tax >= 100) {
                        taxInterest.setText(Integer.toString(100 - 1));
                    }
                    inStrLength = taxInterest.getText().toString().length();
                    sendUotData();
                }
                if (!GeneralValidator.validate(taxInterest) && inStrLength > 0) {
                    taxInterest.setText("0");
                    inStrLength = 0;
                }
            }
        });

        capitalCh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sendUotData();
            }
        });
        return view;
    }

    private void sendUotData() {
        Bundle outData = new Bundle();
        DepositCalc calc = new DepositCalc(
                GeneralValidator.validateDouble(inAmountText),
                GeneralValidator.validateDouble(interestRateText),
                GeneralValidator.validateDouble(periodOfTimeText),
                GeneralValidator.validateDouble(depositReplenishmentText),
                GeneralValidator.validateDouble(taxInterest),
                capitalCh.isChecked()
        );
        outData.putDouble(InfoOutDepositFragment.EXTRA_MONTHLY_INCOME, calc.getMonthlyIncome());
        outData.putInt(InfoOutDepositFragment.EXTRA_TERM, (int) (GeneralValidator.validateDouble(periodOfTimeText)));
        outData.putDouble(InfoOutDepositFragment.EXTRA_GENERAL_INCOME_N_MONTHS, calc.getTotalIncomeInNMonths());
        outData.putDouble(InfoOutDepositFragment.EXTRA_GENERAL_SUM_OF_DEPOSIT, calc.getReplenishmentAmount());
        outData.putDouble(InfoOutDepositFragment.EXTRA_TOTAL_AMOUNT, calc.getTotalAmount());

        dataSender.acceptMessage(outData);
    }
}